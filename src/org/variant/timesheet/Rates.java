package org.variant.timesheet;
import static org.variant.timesheet.Slot.*;

public enum Rates {
    MONDAY{
        @Override
        public String toString() {
            return this.name() + " ";
        }
    },
    TUESDAY{
        @Override
        public String toString() {
            return this.name()+ " ";
        }
    },
    WEDNESDAY{
        @Override
        public String toString() {
            return this.name()+ " ";
        }
    },
    THURSDAY{
        @Override
        public String toString() {
            return this.name()+ " ";
        }
    },
    FRIDAY{
        @Override
        public String toString() {
            return this.name() + "\nnormalHourlyRate (between "+WORKDAY_START_HOUR+" and "+WORKDAY_END_HOUR +"): "+ getNormalHourlyRate() +
                    " eur/hour\novertimeHourlyRate (between "+WORKDAY_END_HOUR+" and "+ WORKDAY_START_HOUR+"): "  + getOvertimeHourlyRate()+" eur/hour\n" ;
        }
    },
    SATURDAY{
        public double getNormalHourlyRate () {
            return normalHourlyRate + 10;
        }
        public double getOvertimeHourlyRate() {
            return getNormalHourlyRate();
        }
        @Override
        public String toString() {
            return this.name()+ "\nHourlyRate: " + getNormalHourlyRate() +" eur/hour\n";
        }
    },
    SUNDAY{
        public double getNormalHourlyRate () {
            return normalHourlyRate + 20;
        }

        public double getOvertimeHourlyRate() {
            return getNormalHourlyRate();
        }
        @Override
        public String toString() {
            return this.name() + "\nHourlyRate: " + getNormalHourlyRate() +" eur/hour";
        }

    },;

    double overtimeHourlyRate = 20;
    double normalHourlyRate = 15;

    public double getOvertimeHourlyRate() {
        return overtimeHourlyRate;
    }

    public double getNormalHourlyRate() {
        return normalHourlyRate;
    }

    public void setOvertimeHourlyRate(double overtimeHourlyRate) {
        this.overtimeHourlyRate = overtimeHourlyRate;
    }

    public void setNormalHourlyRate(double normalHourlyRate) {
        this.normalHourlyRate = normalHourlyRate;
    }

    @Override
    public String toString() {
        return "";
    }
}
