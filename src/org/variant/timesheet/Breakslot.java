package org.variant.timesheet;
import java.time.Duration;
import java.time.LocalTime;

public class Breakslot implements  Slot {
    @Override
    public LocalTime getEnd() {
        return end;
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    private String weekendDay;
    private long[] minutesByType = new long[]{0, 0};
    private LocalTime end;
    private LocalTime start;

    public Breakslot(LocalTime start, LocalTime end) {
        this.end = end;
        this.start = start;
        calculateMinutesDayHour(start, end);
    }

    public Breakslot(LocalTime end, LocalTime start, String weekendDay) {
        this.weekendDay = weekendDay;
        this.end = end;
        this.start = start;
        calculateMinutesByTypeWeekend();
    }

    private void calculateMinutesByTypeWeekend(){
        long elapsedMinutes = Duration.between(start, end).toMinutes();
        minutesByType = new long[]{0, 0};
        minutesByType[0] = elapsedMinutes;
    }

    private void calculateMinutesDayHour(LocalTime start,LocalTime end){
        if ((start.compareTo(WORKDAY_START_HOUR) < 0 && end.compareTo(WORKDAY_START_HOUR) <= 0)
                || start.compareTo(WORKDAY_END_HOUR) >= 0 && end.compareTo(WORKDAY_END_HOUR) > 0) {
            calculateMinutesOverHour(start, end);
        } else if (start.compareTo(WORKDAY_START_HOUR) < 0 && end.compareTo(WORKDAY_START_HOUR) > 0) {
            if (end.compareTo(WORKDAY_END_HOUR) <= 0) {
                calculateMinutesOverHour(start, WORKDAY_START_HOUR);
                calculateMinutesDayHour(WORKDAY_START_HOUR, end);
            } else {
                calculateMinutesOverHour(start, WORKDAY_START_HOUR);
                calculateMinutesDayHour(WORKDAY_START_HOUR, WORKDAY_END_HOUR);
                calculateMinutesOverHour(WORKDAY_END_HOUR, end);
            }
        } else if (start.compareTo(WORKDAY_START_HOUR) >= 0 && end.compareTo(WORKDAY_END_HOUR) > 0) {
            calculateMinutesDayHour(start, WORKDAY_END_HOUR);
            calculateMinutesOverHour(WORKDAY_END_HOUR, end);
        } else {
            long elapsedMinutes = Duration.between(start, end).toMinutes();
            minutesByType[0] += elapsedMinutes;
        }
    }

    private void calculateMinutesOverHour(LocalTime start,LocalTime end){
        long elapsedMinutes = Duration.between(start, end).toMinutes();
        minutesByType[1] += elapsedMinutes;
    }

    public long[] getMinutesByType() {
        return minutesByType;
    }

    @Override
    public String toString() {
        return "{" +
                "start=" +start +
                ", end=" +  end +
                '}';
    }

}

