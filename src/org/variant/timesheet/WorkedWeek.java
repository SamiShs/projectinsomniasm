package org.variant.timesheet;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class WorkedWeek implements Week {
    private Day[] myWorkWeekDays = new WorkedDay[7];
    private String firstDayOfTheWeek;
    private Day dayForAddingOrRemovingSlots;
    private Slot workSlotForAddingBreak;


    public WorkedWeek(String firstday) {
        this.firstDayOfTheWeek = firstday;
        fillWorkWeek();
        System.out.println("hello");
    }

    @Override
    public String getFirstDayOfTheWeek() {
        return firstDayOfTheWeek;
    }


    @Override
    public void addWorkSlotToDay(String startTime, String endTime) throws TimeSheetInputExceptions {
        DateTimeFormatter formatOfUserTimeInput = DateTimeFormatter.ofPattern("H:m");
        LocalTime startTimeFormatted;
        LocalTime endTimeFormatted;
        try{
            startTimeFormatted = LocalTime.parse(startTime, formatOfUserTimeInput);
            endTimeFormatted = LocalTime.parse(endTime, formatOfUserTimeInput);
        }catch(DateTimeParseException eeee){
            throw new TimeSheetInputExceptions("Invalid Timeformat. Pls enter in this format hour:minutes for example 17:44");
        }
        if (endTimeFormatted.compareTo(startTimeFormatted) <= 0) {
            throw new TimeSheetInputExceptions("your end time is before or same as start time");
        }
        if (getDayForAddingOrRemovingSlots().getWeekDayObject() == Rates.SATURDAY || getDayForAddingOrRemovingSlots().getWeekDayObject() == Rates.SUNDAY) {
            getDayForAddingOrRemovingSlots().addSlot(new TimeSlot(startTimeFormatted, endTimeFormatted, Rates.SATURDAY.name()));
        } else {
            getDayForAddingOrRemovingSlots().addSlot(new TimeSlot(startTimeFormatted, endTimeFormatted));
        }
    }
    @Override
    public void addBreakSlotToDay(String startTime, String endTime) {
        DateTimeFormatter formatOfUserTimeInput = DateTimeFormatter.ofPattern("H:m");
        LocalTime startTimeFormatted;
        LocalTime endTimeFormatted;
        try{
            startTimeFormatted = LocalTime.parse(startTime, formatOfUserTimeInput);
            endTimeFormatted = LocalTime.parse(endTime, formatOfUserTimeInput);
        }catch(DateTimeParseException eeee){
            throw new TimeSheetInputExceptions("Invalid Timeformat. Pls enter in this format hour:minutes for example 17:44");
        }
        if (endTimeFormatted.compareTo(startTimeFormatted) <= 0) {
            throw new TimeSheetInputExceptions("your end time is before or same as start time");
        }
        if (!(workSlotForAddingBreak.getStart().compareTo(startTimeFormatted) <= 0 && workSlotForAddingBreak.getEnd().compareTo(startTimeFormatted) >= 0 &&
                workSlotForAddingBreak.getStart().compareTo(endTimeFormatted) <= 0 && workSlotForAddingBreak.getEnd().compareTo(endTimeFormatted) >= 0)) {
            throw new TimeSheetInputExceptions("Time is not within the given slot");
        }
        if (getDayForAddingOrRemovingSlots().getWeekDayObject() == Rates.SATURDAY) {
            getDayForAddingOrRemovingSlots().addSlot(new Breakslot(startTimeFormatted, endTimeFormatted, Rates.SATURDAY.name()));
        } else if (getDayForAddingOrRemovingSlots().getWeekDayObject() == Rates.SUNDAY) {
            getDayForAddingOrRemovingSlots().addSlot(new Breakslot(startTimeFormatted, endTimeFormatted, Rates.SUNDAY.name()));
        } else {
            getDayForAddingOrRemovingSlots().addSlot(new Breakslot(startTimeFormatted, endTimeFormatted));
        }
    }

    @Override
    public void setDayForAddingOrRemovingSlots(Day day) {
        this.dayForAddingOrRemovingSlots = day;
    }

    @Override
    public Day getDayForAddingOrRemovingSlots() {
        return this.dayForAddingOrRemovingSlots;
    }

    @Override
    public Slot[] getAllSlotsForTheDay() {
        return getDayForAddingOrRemovingSlots().getTimeSlots();
    }

    @Override
    public void removeSlotFromDay(Slot slot) {
        getDayForAddingOrRemovingSlots().removeSlot(slot);
    }

    @Override
    public Day[] getAllWeekDays() {
        return myWorkWeekDays;
    }

    private void fillWorkWeek() throws TimeSheetInputExceptions{
        DateTimeFormatter mijnFor = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate givenDateFormatted;
        try{
            givenDateFormatted = LocalDate.parse(firstDayOfTheWeek, mijnFor);
        }catch(DateTimeParseException ddd){
            throw new TimeSheetInputExceptions("Your timeformat is incorrect, please use the following format day/month/year (10/10/2011)");
        }

        for (int i = 0; i < 7; i++) {
            myWorkWeekDays[i] = new WorkedDay(givenDateFormatted);
            givenDateFormatted = givenDateFormatted.plusDays(1);
        }
    }

    public void printSlotsMenuForTheDay(Boolean isRomoveSlot){
        if(getAllSlotsForTheDay() == null){
            throw new TimeSheetInputExceptions("You dont have any slots");
        }
        int i = 0;
        for (int slotsToPrint = 0; slotsToPrint < getAllSlotsForTheDay().length; slotsToPrint++) {
            if (isRomoveSlot || getAllSlotsForTheDay()[slotsToPrint] instanceof TimeSlot) {
                i++;
                System.out.println(i+ " " + getAllSlotsForTheDay()[slotsToPrint]);
            }
        }
        int chosenSlotNumber = Keyboard.askForNumber("choose the corresponding number of the slot: ");
        int j=0;
        for (i = 0; i < getAllSlotsForTheDay().length; i++) {
            if (j == chosenSlotNumber) {
                chosenSlotNumber = i;
                break;
            } else if (getAllSlotsForTheDay()[i] instanceof TimeSlot) {
                j++;
            }
        }
        if (chosenSlotNumber < 1 || chosenSlotNumber > getAllSlotsForTheDay().length) {
            throw new TimeSheetInputExceptions("Chose a number between 1 and " + getAllSlotsForTheDay().length);
        }
        if (isRomoveSlot) {
            removeSlotFromDay(getAllSlotsForTheDay()[chosenSlotNumber - 1]);
            System.out.println("slot removed!\n");
        } else {
            workSlotForAddingBreak = getAllSlotsForTheDay()[chosenSlotNumber - 1];
        }
    }

    @Override
    public void printDaysMenu() throws TimeSheetInputExceptions {
        for (int dayToPrint = 0; dayToPrint < getAllWeekDays().length; dayToPrint++) {
            System.out.println(dayToPrint + 1 + " " + getAllWeekDays()[dayToPrint]);
        }
        int chosenDay = Keyboard.askForNumber("choose the corresponding number of the day you want to add slot to: ");
        if ( chosenDay < 1 || chosenDay > 7) {
            throw new TimeSheetInputExceptions("Choice invalid. Please chose number from 1 to 7");
        }
        setDayForAddingOrRemovingSlots(getAllWeekDays()[chosenDay - 1]);
    }

    @Override
    public void addSlotToDay() {
        String chosenSlotType = Keyboard.askForText("\ntype w for workslot and b for break slot: ");
        if (!chosenSlotType.equals("w") && !chosenSlotType.equals("b")) {
            throw new  TimeSheetInputExceptions("Choice invalid: choose between w or b");
        }
        if (chosenSlotType.equals("b")) {
            printSlotsMenuForTheDay(false);
        }
        String givenStartTime = Keyboard.askForText("\ngive in start time (H:M): ");
        String givenEndTime = Keyboard.askForText("give in end time (H:M): ");
        if (chosenSlotType.equals("w")) {
            addWorkSlotToDay(givenStartTime, givenEndTime);
        } else {
            addBreakSlotToDay(givenStartTime, givenEndTime);
        }
        System.out.println("slot added!\n");
    }

    @Override
    public String simplePayCheck() {
        StringBuilder payCheckPrintout = new StringBuilder("");
        for (int indexOfDay = 0; indexOfDay < getAllWeekDays().length; indexOfDay++) {
            payCheckPrintout.append(String.format("%2d %-9s %s  %s %4d:%02d  %s  %10.2f euro%n", indexOfDay + 1, getAllWeekDays()[indexOfDay].getWeekDayObject().name(), getAllWeekDays()[indexOfDay].getDate()
                    , "Total time worked:", getAllWeekDays()[indexOfDay].totalWorkedMinutes() / 60, getAllWeekDays()[indexOfDay].totalWorkedMinutes() % 60, "Total amount earned:",
                    getAllWeekDays()[indexOfDay].getTotalAmounEarned() / 60));
        }
        return payCheckPrintout.toString();
    }
    public String detailedPayCheck() {
        StringBuilder payCheckPrintout = new StringBuilder("");
        for (int indexOfDay = 0; indexOfDay < getAllWeekDays().length; indexOfDay++) {
            if (getAllWeekDays()[indexOfDay].getTimeSlots() != null) {
                String workSlotPrintout = "";
                String breakSlotPrintout = "";
                for (Slot slot : getAllWeekDays()[indexOfDay].getTimeSlots()) {
                    if (slot instanceof TimeSlot) {
                        workSlotPrintout += slot.toString() + " >< ";
                    } else {
                        breakSlotPrintout += slot.toString() + " >< ";
                    }
                }
                if (!breakSlotPrintout.equals("")) {
                    breakSlotPrintout =  breakSlotPrintout.substring(0, breakSlotPrintout.length() - 3);
                }
                payCheckPrintout.append(String.format("%-9s %s:%n  %s%s%n  %s%s%n",
                        getAllWeekDays()[indexOfDay].getWeekDayObject().name(), getAllWeekDays()[indexOfDay].getDate()
                        , "Workslots: ", workSlotPrintout.substring(0, workSlotPrintout.length() - 3),
                        "Breakslots: ", breakSlotPrintout
                ));
                if (getAllWeekDays()[indexOfDay].getWeekDayObject() == Rates.SATURDAY || getAllWeekDays()[indexOfDay].getWeekDayObject() == Rates.SUNDAY) {
                    payCheckPrintout.append(String.format("  Weekend hour: total of %d hours and %02d minutes worked at wage %.2f euro/hour%n%n",
                                    getAllWeekDays()[indexOfDay].getMinutesByType()[0]/ 60+0, getAllWeekDays()[indexOfDay].getMinutesByType()[0] % 60+0,
                            getAllWeekDays()[indexOfDay].getWeekDayObject().getNormalHourlyRate()));

                } else {
                    payCheckPrintout.append(String.format("  Day hour: total of %d hours and %02d minutes worked at wage %.02f euro/hour%n" +
                                    "  Outside day hour: Total of %d hours and %02d minutes worked at wage %.02f euro/hour%n%n",
                            getAllWeekDays()[indexOfDay].getMinutesByType()[0] / 60+0, getAllWeekDays()[indexOfDay].getMinutesByType()[0] % 60+0,
                            getAllWeekDays()[indexOfDay].getWeekDayObject().getNormalHourlyRate(),
                            getAllWeekDays()[indexOfDay].getMinutesByType()[1] / 60+0, getAllWeekDays()[indexOfDay].getMinutesByType()[1]% 60+0,
                            getAllWeekDays()[indexOfDay].getWeekDayObject().getOvertimeHourlyRate()));
                    }
            } else {
                payCheckPrintout.append(String.format("%-9s %s:%n  No slots!%n%n",
                        getAllWeekDays()[indexOfDay].getWeekDayObject().name(), getAllWeekDays()[indexOfDay].getDate()));
            }
        }
        return payCheckPrintout.toString();
    }
}
