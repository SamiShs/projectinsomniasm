package org.variant.timesheet;

/**
 * The interface Week service.
 */
public interface WeekService {
    /**
     * Reset.
     */
    void reset();


    /**
     * Create week.
     *
     * @param day the day
     */
    void createWeek(String day);

    /**
     * Gets workedweek.
     *
     * @return the workedweek
     */
    Week getWorkedweek();

    /**
     * Get all week days day [ ].
     *
     * @return the day [ ]
     */
    Day [] getAllWeekDays();

    /**
     * Get all slots for the day slot [ ].
     *
     * @return the slot [ ]
     */
    Slot[] getAllSlotsForTheDay();
}
