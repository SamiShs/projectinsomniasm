package org.variant.timesheet;

public interface Week {

    Day[] getAllWeekDays();

    void addWorkSlotToDay(String startTime, String endTime);

    void addBreakSlotToDay(String starTime, String endTIme);

    void removeSlotFromDay(Slot slot);

    void setDayForAddingOrRemovingSlots(Day day);

    String getFirstDayOfTheWeek();

    Day getDayForAddingOrRemovingSlots();

    Slot[] getAllSlotsForTheDay();

    String simplePayCheck();

    String detailedPayCheck();

    void printSlotsMenuForTheDay(Boolean isRemovedSlot);

    void printDaysMenu();

    void addSlotToDay();


}
