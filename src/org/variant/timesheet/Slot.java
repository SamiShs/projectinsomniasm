package org.variant.timesheet;

import java.time.LocalTime;

/**
 * The interface Slot.
 */
public interface Slot {
    /**
     * The constant WORKDAY_START_HOUR.
     */
    LocalTime WORKDAY_START_HOUR = LocalTime.of(8, 0);
    /**
     * The constant WORKDAY_END_HOUR.
     */
    LocalTime WORKDAY_END_HOUR = LocalTime.of(18, 0);

    /**
     * Get minutes by type long [ ].
     *
     * @return the long [ ]
     */
    long[] getMinutesByType();

    LocalTime getEnd();

    LocalTime getStart();

}
