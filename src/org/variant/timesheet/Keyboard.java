package org.variant.timesheet;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Keyboard {

    private static Scanner keyboard = new Scanner(System.in);

    public static String askForText(String message) {
        System.out.print(message);
        String userStringInput = keyboard.next();
        return userStringInput;
    }

    public static int askForNumber(String message) throws TimeSheetInputExceptions {
        System.out.print(message);
        int userIntegerInput = 0;
        try{
            userIntegerInput = keyboard.nextInt();
            System.out.println("\n");
        } catch (Exception ee) {
            keyboard = new Scanner(System.in);
            throw new TimeSheetInputExceptions("Invalid number. Please put in a number");
        }
        return userIntegerInput;
    }

}
