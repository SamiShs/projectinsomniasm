package org.variant.timesheet;
import java.time.Duration;
import java.time.LocalTime;

public class TimeSlot implements Slot{
    private String weekendDay;
    private long[] minutesByType = new long[]{0, 0};
    private LocalTime end;
    private LocalTime start;

    public TimeSlot(LocalTime start, LocalTime end) {
        this.end = end;
        this.start = start;
        calculateMinutesDayHour(start,end);
    }

    public TimeSlot(LocalTime start, LocalTime end, String weekendDay) {
        this.weekendDay = weekendDay;
        this.end = end;
        this.start = start;
        calculateMinutesByTypeWeekend(start,end);
    }

    private void calculateMinutesByTypeWeekend(LocalTime start,LocalTime end){
        long elapsedMinutes = Duration.between(start, end).toMinutes();
        minutesByType[0] = elapsedMinutes;
    }

    private void calculateMinutesDayHour(LocalTime startTime,LocalTime endTime){
        if ((startTime.compareTo(WORKDAY_START_HOUR) < 0 && endTime.compareTo(WORKDAY_START_HOUR) <= 0)
                || startTime.compareTo(WORKDAY_END_HOUR) >= 0 && endTime.compareTo(WORKDAY_END_HOUR) > 0) {
            calculateMinutesOverHour(startTime, endTime);
        } else if (startTime.compareTo(WORKDAY_START_HOUR) < 0 && endTime.compareTo(WORKDAY_START_HOUR) > 0) {
            if (endTime.compareTo(WORKDAY_END_HOUR) <= 0) {
                calculateMinutesOverHour(startTime, WORKDAY_START_HOUR);
                calculateMinutesDayHour(WORKDAY_START_HOUR, endTime);
            } else {
                calculateMinutesOverHour(startTime, WORKDAY_START_HOUR);
                calculateMinutesDayHour(WORKDAY_START_HOUR, WORKDAY_END_HOUR);
                calculateMinutesOverHour(WORKDAY_END_HOUR, endTime);
            }
        } else if (startTime.compareTo(WORKDAY_START_HOUR) >= 0 && endTime.compareTo(WORKDAY_END_HOUR) > 0) {
            calculateMinutesDayHour(startTime, WORKDAY_END_HOUR);
            calculateMinutesOverHour(WORKDAY_END_HOUR, endTime);
        } else {
            long elapsedMinutes = Duration.between(startTime, endTime).toMinutes();
            minutesByType[0] += elapsedMinutes;
        }
    }

    private void calculateMinutesOverHour(LocalTime start,LocalTime end){
        long elapsedMinutes = Duration.between(start, end).toMinutes();
        minutesByType[1] += elapsedMinutes;
    }

    public long[] getMinutesByType() {
        return minutesByType;
    }

    public LocalTime getEnd() {
        return end;
    }

    public LocalTime getStart() {
        return start;
    }

    @Override
    public String toString() {
        return "{" +
                "start=" + start +
                ", End=" + end +
                '}';
    }
}
