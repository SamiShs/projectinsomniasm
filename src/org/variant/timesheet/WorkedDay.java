package org.variant.timesheet;

import java.time.Duration;
import java.time.LocalDate;

public class WorkedDay implements Day {
    private LocalDate date;
    private Slot[] timeSlots;
    private long[] minutesByType = new long[]{0, 0};

    public WorkedDay(LocalDate date) {
        this.date = date;
    }

    @Override
    public LocalDate getDate() {
        return date;
    }

    @Override
    public Slot[] getTimeSlots() {
        return timeSlots;
    }

    @Override
    public Rates getWeekDayObject() {
        return Rates.values()[getDate().getDayOfWeek().getValue()-1];
    }

    @Override
    public void removeSlot(Slot slot) {
        if (timeSlots != null) {
            Slot[] temparray = new Slot[getTimeSlots().length-1];
            int j = 0;
            for (int i = 0; i < temparray.length; i++) {
                if (getTimeSlots()[i] == slot) {
                    j++;
                }
                temparray[i] = getTimeSlots()[j];
                j++;
            }
            this.timeSlots = temparray;
            for (int i = 0; i < 4; i++) {
                minutesByType[i] -= slot.getMinutesByType()[i];
            }
        }
    }

    @Override
    public void addSlot(Slot slot) throws TimeSheetInputExceptions {
        if (timeSlots == null) {
            timeSlots = new Slot[]{slot};
        } else {
            if (isOverlappingOtherSlot(slot)) {
                throw new TimeSheetInputExceptions("You have inserted an overlapping hour!");
            }
            Slot[] temparry = new Slot[timeSlots.length+1];
            int i;
            for (i = 0; i < timeSlots.length; i++) {
                temparry[i] = timeSlots [i];
            }
            temparry[i] = slot;
            timeSlots = temparry;
        }
        if (slot instanceof TimeSlot) {
            for (int i = 0; i < 2; i++) {
                minutesByType[i] += slot.getMinutesByType()[i];
            }
        } else {
            for (int i = 0; i < 2; i++) {
                minutesByType[i] -= slot.getMinutesByType()[i];
            }
        }
    }

    private Boolean isOverlappingOtherSlot(Slot slotToAdd) {
        for (Slot slotInDay : getTimeSlots()) {
            if (slotToAdd.getClass() == slotInDay.getClass()) {
                long minutesBetweenStartNewSlotEndOldSlot = Duration.between(slotToAdd.getStart(), slotInDay.getEnd()).toMinutes();
                long minutesBetweenStartOldSlotEndOldSlot = Duration.between(slotInDay.getStart(), slotInDay.getEnd()).toMinutes();
                if (minutesBetweenStartNewSlotEndOldSlot <= minutesBetweenStartOldSlotEndOldSlot && minutesBetweenStartNewSlotEndOldSlot>0) {
                    return true;
                }
                long minutesBetweenStartOldSlotEndNewSlot = Duration.between(slotInDay.getStart(),slotToAdd.getEnd()).toMinutes();
                if (minutesBetweenStartOldSlotEndNewSlot <= minutesBetweenStartOldSlotEndOldSlot && minutesBetweenStartOldSlotEndNewSlot>0) {
                    return true;
                }
                if (minutesBetweenStartNewSlotEndOldSlot > 0 && minutesBetweenStartOldSlotEndNewSlot > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public long totalWorkedMinutes() {
        long totalMinutes = 0;
        for (long minutes : minutesByType) {
            totalMinutes += minutes;
        }
        return totalMinutes;
    }
    @Override
    public long[] getMinutesByType() {
        return minutesByType;
    }

    @Override
    public double getTotalAmounEarned() {
        return minutesByType[0]* getWeekDayObject().getNormalHourlyRate()+minutesByType[1]* getWeekDayObject().getOvertimeHourlyRate();
    }

    @Override
    public String toString() {
        return  getWeekDayObject().name() + " " + date.toString();

    }
}
