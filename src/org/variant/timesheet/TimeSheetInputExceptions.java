package org.variant.timesheet;

public class TimeSheetInputExceptions extends RuntimeException {
    public TimeSheetInputExceptions() {
    }

    public TimeSheetInputExceptions(String message) {
        super(message);
    }

    public TimeSheetInputExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public TimeSheetInputExceptions(Throwable cause) {
        super(cause);
    }

    public TimeSheetInputExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
