package org.variant.timesheet;

public class WeekServiceImpl implements WeekService {
    private Week workedweek;

    @Override
    public void reset() {
        createWeek(getWorkedweek().getFirstDayOfTheWeek());
    }

    @Override
    public void createWeek(String firstWeekDay) {
        workedweek = new WorkedWeek(firstWeekDay);
    }

    public Week getWorkedweek() {
        return workedweek;
    }

    @Override
    public Day[] getAllWeekDays() {
        return getWorkedweek().getAllWeekDays();
    }

    @Override
    public Slot[] getAllSlotsForTheDay() {
        return getWorkedweek().getAllSlotsForTheDay();
    }
}
