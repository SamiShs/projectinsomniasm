package org.variant.timesheet;

import java.time.LocalDate;

/**
 * The interface Day.
 */
public interface Day {

    /**
     * Remove slot.
     *
     * @param slot the slot
     */
    void removeSlot(Slot slot);

    /**
     * Add slot.
     *
     * @param slot the slot
     */
    void addSlot(Slot slot);

    /**
     * Total worked minutes long.
     *
     * @return the long
     */
    long totalWorkedMinutes();

    /**
     * Gets total amoun earned.
     *
     * @return the total amoun earned
     */
    double getTotalAmounEarned();

    /**
     * Gets date.
     *
     * @return the date
     */
    LocalDate getDate();

    /**
     * Get time slots slot [ ].
     *
     * @return the slot [ ]
     */
    Slot[] getTimeSlots();

    /**
     * Get minutes by type long [ ].
     *
     * @return the long [ ]
     */
    long[] getMinutesByType();

    /**
     * Gets week day object.
     *
     * @return the week day object
     */
    Rates getWeekDayObject();


}
