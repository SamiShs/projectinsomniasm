package org.variant.timesheet;

import java.util.Arrays;

public class Menu {
     private WeekServiceImpl weekService = null;
     final int QUIT_NUMBER = 8;

    public void bootUpMenu(){
         System.out.println("Welcome to our Time Sheet App \nWe are booting up the system for you");
         System.out.println("+++++     =====     +++++");
         System.out.println("+++++     =====     +++++");
         mainMenu();

     }
    public void closedDownMenu(){
        System.out.println("Thx for using the app, till next time!");
    }


    public void mainMenu(){
        try{
            while (true) {
                System.out.println("What do you want to do?");
                System.out.println("Type in the number that corresponds with your choice");
                System.out.println("1. Show the different Hourly rates");
                System.out.println("2. Start a new workweek");
                System.out.println("3. Add a worked moment or break");
                System.out.println("4. Remove a worked moment or break");
                System.out.println("5. Reset");
                System.out.println("6. Print Paycheck");
                System.out.println("7. Print detailed Paycheck");
                System.out.println("8. Quit application");

                int chosenMenuNumber = Keyboard.askForNumber("give number: ");
                if (chosenMenuNumber < 1 || chosenMenuNumber > 8) {
                    throw new TimeSheetInputExceptions("Choice invalid. Please chose number from 1 to 8");
                }
                if (chosenMenuNumber > 2 && chosenMenuNumber < 8) {
                    if(weekService== null || weekService.getWorkedweek() == null){
                        throw new TimeSheetInputExceptions("First create week to add workslot");
                    }
                }
                switch (chosenMenuNumber) {
                    case 1:
                        System.out.println(Arrays.toString(Rates.values()).replace("[","").replace("]","").replace(",",""));
                        Keyboard.askForText("type y to return to MainMenu: ");
                        break;
                    case QUIT_NUMBER:
                        break;
                    case 2:
                        String givenFirstDayWorkedWeek = Keyboard.askForText("give in first day of the week (dd/mm/yyyy): ");
                        weekService = new WeekServiceImpl();
                        weekService.createWeek(givenFirstDayWorkedWeek);
                        System.out.println("** new week created **\n");
                        break;
                    case 3:
                        inBetween(weekService,true);
                        break;
                    case 4:
                        inBetween(weekService,false);
                        break;
                    case 5:
                        weekService.reset();
                        break;
                    case 6:
                        System.out.println(weekService.getWorkedweek().simplePayCheck());
                        break;
                    case 7:
                        System.out.println(weekService.getWorkedweek().detailedPayCheck());
                        break;

                }
                if (chosenMenuNumber == QUIT_NUMBER) {
                    closedDownMenu();
                    break;
                }
            }

        }catch(TimeSheetInputExceptions tiet){
            System.out.println("\n"+tiet.getMessage()+"\n");
            mainMenu();
        }
    }

    public void inBetween(WeekServiceImpl weekService, Boolean isAddSlotOrRemove) throws TimeSheetInputExceptions{
        weekService.getWorkedweek().printDaysMenu();
        if (isAddSlotOrRemove) {
            weekService.getWorkedweek().addSlotToDay();
        } else {
            weekService.getWorkedweek().printSlotsMenuForTheDay(!isAddSlotOrRemove);
        }
    }
}
